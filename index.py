#!/usr/bin/env python3

import serial
import time

def set_address(s, adr):
    """
    Change primary mbus address with message.
    """
    head = b'\x68\x06\x06\x68'
    body = b'\x53\xFE\x51\x01\x7A' # FE - broadcast address
    end = b'\x16'

    # checksum
    cksm = bytes([(sum(body) + adr).to_bytes(2, byteorder='big')[-1]])

    # bytes of message
    SET_ADDRESS = head + body + bytes([adr]) + cksm + end # address FE = 254 = broadcast on MBus line

    # entrance message
    print('Setting address to {} ... {}'.format(
        adr,
        ' '.join('{:02x}'.format(i) for i in SET_ADDRESS)
    ))

    # send to serial line
    s.write(SET_ADDRESS)

    # msg
    print('Waiting for responses ... for a 10 seconds.')

    # wait for response 10 seconds
    time.sleep(10)

    if s.in_waiting:
        # read buffered messages
        temp = ser.read(ser.in_waiting)

        # convert bytes to hexadecimal strings - to be correctly printed.
        temp = ' '.join('{:02x}'.format(i) for i in temp)

        print('Printing received responses: {}'.format(temp))
    else:
        print('No responses on MBus line.')


def serial_mbus_snd_nke_broadcast(s):
    """
    Sends SND_NKE broadcast message to MBus line.
    """
    # bytes of message
    SND_NKE_BROADCAST = b'\x10\x40\xFE\x3E\x16' # address FE = 254 = broadcast on MBus line

    # entrance message
    print('Sending MBus broadcast SND_NKE message ... {}'.format(
        ' '.join('{:02x}'.format(i) for i in SND_NKE_BROADCAST)
    ))

    # send to serial line
    s.write(SND_NKE_BROADCAST)

    # msg
    print('Waiting for responses ... for a 10 seconds.')

    # wait for response 10 seconds
    time.sleep(10)

    if s.in_waiting:
        # read buffered messages
        temp = ser.read(ser.in_waiting)

        # convert bytes to hexadecimal strings - to be correctly printed.
        temp = ' '.join('{:02x}'.format(i) for i in temp)

        print('Printing received responses: {}'.format(temp))
    else:
        print('No responses on MBus line.')


def serial_mbus_rsp_ud_broadcast(s):
    """
    Sends SND_NKE broadcast message to MBus line.
    """
    # bytes of message
    RSP_UD_BROADCAST = b'\x10\x5B\xFE\x59\x16' # address FE = 254 = broadcast on MBus line

    # entrance message
    print('Sending MBus broadcast RSP_UD message ... {}'.format(
        ' '.join('{:02x}'.format(i) for i in RSP_UD_BROADCAST)
    ))

    # send to serial line
    s.write(RSP_UD_BROADCAST)

    # msg
    print('Waiting for responses ... for a 2 seconds.')

    # wait for response 10 seconds
    time.sleep(5)

    if s.in_waiting:
        # read buffered messages
        temp = ser.read(ser.in_waiting)

        # convert bytes to hexadecimal strings - to be correctly printed.
        temp = ' '.join('{:02x}'.format(i) for i in temp)

        print('Printing received responses: {}'.format(temp))
    else:
        print('No responses on MBus line.')


def serial_mbus_snd_nke(s, address):
    """
    Sends SND_NKE message to primary address Slave on MBus line.
    """

    # checksum
    cksm = bytes([(address + 0x40).to_bytes(2, byteorder='big')[-1]])

    # bytes of message
    SND_NKE = b'\x10\x40' + bytes([address]) + cksm + b'\x16'

    # entrance message
    print('Sending MBus SND_NKE message to {} slave ... {}'.format(
        address,
        ' '.join('{:02x}'.format(i) for i in SND_NKE)
    ))

    # send to serial line
    s.write(SND_NKE)

    # msg
    print('Waiting for response from {} ... for a 10 seconds.'.format(address))

    # wait for response 10 seconds
    time.sleep(10)

    if s.in_waiting:
        # read buffered messages
        temp = ser.read(ser.in_waiting)

        # convert bytes to hexadecimal strings - to be correctly printed.
        temp = ' '.join('{:02x}'.format(i) for i in temp)

        print('Printing received response from slave with address of {}:\n{}'.format(
            address,
            temp
        ))
    else:
        print('No response from slave with address of {}.'.format(address))


def serial_mbus_rsp_ud(s, address):
    """
    Sends RSP_UD broadcast message to MBus line.
    """

    # checksum
    cksm = bytes([(address + 0x5B).to_bytes(2, byteorder='big')[-1]])

    # bytes of message
    RSP_UD = b'\x10\x5B' + bytes([address]) + cksm + b'\x16'

    # entrance message
    print('Sending MBus RSP_UD message to slave of {} address ... {}'.format(
        address,
        ' '.join('{:02x}'.format(i) for i in RSP_UD)
    ))

    # send to serial line
    s.write(RSP_UD)

    # msg
    print('Waiting for responses ... for a 10 seconds.')

    # wait for response 10 seconds
    time.sleep(10)

    if s.in_waiting:
        # read buffered messages
        temp = ser.read(ser.in_waiting)

        # convert bytes to hexadecimal strings - to be correctly printed.
        temp = ' '.join('{:02x}'.format(i) for i in temp)

        print('Printing received responses: {}'.format(temp))
    else:
        print('No responses on MBus line.')


def scan_network():
    """
    for i in range(254, 255):
        cksm = bytes([(i + 0x7B).to_bytes(2, byteorder='big')[-1]])
        msg_SND_NKE = b'\x10\x7B' + bytes([i]) + cksm + b'\x16'
        msg = ' '.join('{:02x}'.format(j) for j in msg_SND_NKE)
        print(msg)

        # ask the address
        ser.write(msg_SND_NKE)
        time.sleep(8) # Cynox timeout = 1 s
        print(ser.in_waiting)
        #temp = ser.read(159) # Cynox response length

        if ser.in_waiting:
            # temp = ser.read(94)
            temp = ser.read(ser.in_waiting)
            temp = ' '.join('{:02x}'.format(i) for i in temp)
            print(i, temp)
    """
    pass


if __name__ == '__main__':
    # setup serial line connection
    ser = serial.Serial(
        '/dev/ttyUSB1',
        2400,
        parity=serial.PARITY_EVEN,
        rtscts=0
    )

    #serial_mbus_snd_nke_broadcast(ser)
    set_address(ser, 1)
    serial_mbus_rsp_ud_broadcast(ser)
    #serial_mbus_rsp_ud(ser, 42)
